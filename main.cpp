
#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include "SX1276.h"

#include <iostream>

using namespace std;


extern int fd_spi;

uint8_t re;

char payload [] = "7418529630"; /* 10 bytes as payload */


SX1276	  sx1276;


int main(int argc, char *argv[])
{
	char re=0;
	int  flag=0;

	if(argc<=1)
	{	cout << "\033[31m  please set the lora sending or recviving mode \033[0m" <<endl;
	 	return 0;
	}
	
	if(0 == strcmp(argv[1], "--send"))
	{
	  flag =1;
	
	}else if(0 == strcmp(argv[1], "--recv")){
       flag =2;
	}else {
		flag=0;
		cout << "Please  set  the lora sending or receiving mode"<< endl;
	}


    if(flag==2){
		/* recv mode */
			sx1276.ON();
			re = sx1276.setMode(1);
			re = sx1276.setChannel(CH_16_868);
			re = sx1276.setPower('M');
			re = sx1276.setNodeAddress(8);
	}else if(flag==1){
		/* sent mode */
			sx1276.ON();
			re = sx1276.setMode(1);
			re = sx1276.setChannel(CH_16_868);
			sx1276.setMaxCurrent(0x1B);
			sx1276.getMaxCurrent();
			re = sx1276.setPower('M');
			re = sx1276.setNodeAddress(3);
	}else{
		;
	}

	while(1)
	{
		if(flag==2){
			/* recv mode */
			re = sx1276.receivePacketTimeout(10000); 
			re = sx1276.getRSSIpacket();
		}else if(flag==1){
			/* sent mode */
			re = sx1276.sendPacketTimeout(8, payload);
		}
		else{
			flag=0;
			std::cout << "error!!! " << std::endl;
			sleep(1);
		}
		usleep(30000);
	}


}



