all: spi_sx1276
.PHONY :all

spi_sx1276: SX1276.o main.o
	g++ -o $@ $^
	
%.o : %.cpp
	g++ -c -o $@ $<

clean:
	rm -f *.o spi_sx1276

